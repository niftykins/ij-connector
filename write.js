var path = require('path');
var fs = require('fs');
var YAML = require('yamljs');

function rmrf(p, leaveDir) {
	if (fs.existsSync(p)) {
		fs.readdirSync(p).forEach(function(file) {
			var curPath = path.join(p, file);

			// recurse
			if (fs.lstatSync(curPath).isDirectory()) {
				return rmrf(curPath);
			}

			// delete file
			fs.unlinkSync(curPath);
		});

		if ( ! leaveDir) fs.rmdirSync(p);
	} else if (leaveDir) {
		fs.mkdirSync(p);
	}
}

function write(paths, folderCategories, vendors) {
	// erase old data
	Object.keys(paths).forEach(function(section) {
		var folder = paths[section];

		rmrf(folder, true);
	});

	// remake all the folder categories that are used
	folderCategories.forEach(function(category) {
		var p = path.join(paths.reviews, category);

		fs.mkdirSync(p);
	});

	vendors.forEach(function(data) {
		var reviewPath = path.join(paths.reviews, data.folderCategory, data.filename);
		var yaml = YAML.stringify(data.info);

		var content = '---\n';
		content += yaml;
		content += '---\n';

		if (data.review) content += data.review;

		fs.writeFileSync(reviewPath, content);

		data.images.forEach(function(image) {
			var imagePath = path.join(paths[image.folder], image.filename);

			var imageData = fs.readFileSync(image.path);
			fs.writeFileSync(imagePath, imageData);
		});
	});
}

module.exports = {
	write: write,
	rmrf: rmrf
};
