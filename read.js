var path = require('path');
var fs = require('fs');
var YAML = require('yamljs');

function getThingsFromDirectory(src, opts) {
	var things = fs.readdirSync(src);
	opts = opts || {};

	var filesOnly = opts.filesOnly;
	var dirsOnly = opts.dirsOnly;

	things = things.map(function(fileName) {
		return path.join(src, fileName);
	});

	if (filesOnly || dirsOnly) {
		var dirCheck = dirsOnly || !filesOnly;

		things = things.filter(function(filePath) {
			var isDir = fs.statSync(filePath).isDirectory();
			return isDir === dirCheck;
		});
	}

	return things;
}

var match = /---\s+/;
function readVendorFile(src) {
	var file = fs.readFileSync(src).toString();

	// remove the opening ---
	file = file.replace(match, '');

	var endOfYAML = file.search(match);
	var yaml = file.substr(0, endOfYAML);
	file = file.substr(endOfYAML);

	// remove the closing ---
	file = file.replace(match, '');

	// review is all that's left
	var review = file;

	return {
		info: YAML.parse(yaml),
		review: review || ''
	};
}

function readReviews(reviewPath) {
	var reviewFolderCategories = getThingsFromDirectory(reviewPath, {dirsOnly: true});
	var vendors = [];

	reviewFolderCategories.forEach(function(folderPath) {
		var folderName = path.basename(folderPath);
		var reviews = getThingsFromDirectory(folderPath, {filesOnly: true});

		var v = reviews.map(function(vendorPath) {
			var vendor = readVendorFile(vendorPath);
			vendor.folderCategory = folderName;
			vendor.filename = path.basename(vendorPath);

			return vendor;
		});

		vendors = vendors.concat(v);
	});

	return vendors;
}

function readImages(imageFolder) {
	var paths = getThingsFromDirectory(imageFolder, {filesOnly: true});

	var images = {};

	paths.forEach(function(imagePath) {
		var filename = path.basename(imagePath);

		images[filename] = fs.readFileSync(imagePath);
	});

	return images;
}

module.exports = function read(paths) {
	return {
		reviews: readReviews(paths.reviews),
		logos: readImages(paths.logos),
		heroes: readImages(paths.heroes),
		webshots: readImages(paths.webshots)
	};
};
