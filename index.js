var w = require('./write');

module.exports = {
	read: require('./read'),
	write: w.write,
	rmrf: w.rmrf
};
